/*
 * MessageHandler.h
 *
 *  Created on: 18 nov 2016
 *      Author: cirod
 */

#pragma once

#include <string>

class NotificationHandler;

#include "Notification.h"

class NotificationHandler {
public:
	NotificationHandler() = default;
	NotificationHandler(const NotificationHandler &) = default;
	virtual ~NotificationHandler() = default;
	virtual void notify(Notification * notification) = 0;
};

