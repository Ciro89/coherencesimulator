/*
 * Event.h
 *
 *  Created on: 18 nov 2016
 *      Author: cirod
 */


#pragma once

class Simulator;
class Event;

#include <ostream>
#include <string>

class Event {
public:
	Event(double time);
	virtual ~Event();
	double getTime() const;

	virtual void execute(Simulator * simulator) = 0;

	virtual std::string toString() const;
	friend std::ostream & operator<< (std::ostream & out, const Event & event);

	class EventComparator {
	public:
		bool operator() (const Event * left, const Event * right) const;
	};

protected:
	const double time;
};


