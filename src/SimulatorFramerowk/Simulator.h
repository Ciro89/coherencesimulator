/*
 * Simulator.h
 *
 *  Created on: 18 nov 2016
 *      Author: cirod
 */


#pragma once


#include "EventsQueue.h"

class Simulator {
public:
	Simulator();
	virtual ~Simulator();

	void scheduleEvent(Event * event);
	bool removeEvent(Event * event);
	void run();
	void run(double maxSimulationTime);
	void executeNextEvent();

	virtual void stop();

	double getTime() const;

	Simulator(const Simulator & simulator) = delete;
	const Simulator& operator=(const Simulator & simulator) = delete;

private:
	double time;
	EventsQueue * eventsQueue;
	bool running = false;

	void executeNextStep();
};



