/*
 * EventsQueue.h
 *
 *  Created on: 18 nov 2016
 *      Author: cirod
 */


#pragma once



#include <queue>

#include "Event.h"

class EventsQueue : public std::priority_queue<Event*, std::vector<Event*, std::allocator<Event*> >, Event::EventComparator > {
public:
	void insert(Event * event);
	bool remove(Event * event);
	Event* top() const;
	void pop();
	bool empty() const;
};


