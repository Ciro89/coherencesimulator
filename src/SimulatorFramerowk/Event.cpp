/*
 * Event.cpp
 *
 *  Created on: 18 nov 2016
 *      Author: cirod
 */


#include <iomanip>
#include <sstream>
#include "Event.h"

Event::Event(double time) : time(time) {}

Event::~Event() {}

double Event::getTime() const {
	return time;
}

std::ostream & operator<< (std::ostream & out, const Event & event) {
	out << std::setw(20) << std::left;
	out << "Time: " << event.time;
	return out;
}

std::string Event::toString() const {
	std::stringstream ss;
	ss << *this;
	return ss.str();
}

bool Event::EventComparator::operator() (const Event * left, const Event * right) const {
	return left->time > right->time;
}

