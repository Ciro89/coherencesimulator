/*
 * Simulator.cpp
 *
 *  Created on: 18 nov 2016
 *      Author: cirod
 */

#include "Simulator.h"


#include <iostream>
#include <cassert>

Simulator::Simulator() : time(0) {
	eventsQueue = new EventsQueue();
}

Simulator::~Simulator() {
	Event * nextEvent;
	while (!eventsQueue->empty()) {
		nextEvent = eventsQueue->top();
		delete nextEvent;
		eventsQueue->pop();
	}
	delete eventsQueue;
}

bool Simulator::removeEvent(Event * event) {
	bool result = eventsQueue->remove(event);
	delete event;
	return result;
}

void Simulator::executeNextStep() {
	Event* nextEvent = eventsQueue->top();
	eventsQueue->pop();
	assert(time <= nextEvent->getTime());
	time = nextEvent->getTime();
	nextEvent->execute(this);
	delete nextEvent;
}

void Simulator::run() {
	running = true;
	while ((! eventsQueue->empty()) && running) {
		executeNextStep();
	}
	running = false;
}


void Simulator::run(double maxSimulationTime) {
	running = true;
	while (! eventsQueue->empty() && time < maxSimulationTime) {
		executeNextStep();
	}
	running = false;
}


void Simulator::executeNextEvent() {
	if (! eventsQueue->empty()) {
		executeNextStep();
	}
}

void Simulator::scheduleEvent(Event * event) {
	assert(event->getTime() >= time);
	eventsQueue->insert(event);
}

double Simulator::getTime() const {
	return time;
}

void Simulator::stop() {
	running = false;
}


