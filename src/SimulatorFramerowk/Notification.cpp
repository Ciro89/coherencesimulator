/*
 * Message.cpp
 *
 *  Created on: 18 nov 2016
 *      Author: cirod
 */

#include "Notification.h"

#include <iomanip>
#include <sstream>

using std::ostream;
using std::string;
using std::stringstream;

Notification::Notification(double time) :
	Event(time),
	simulator(nullptr),
	sender(nullptr),
	recipient(nullptr) {}

Notification::Notification(double time, NotificationHandler * recipient) :
	Event(time),
	simulator(nullptr),
	sender(nullptr),
	recipient(recipient) {}

Notification::Notification(double time, void * sender, NotificationHandler * recipient) :
	Event(time),
	simulator(nullptr),
	sender(sender),
	recipient(recipient) {}

Notification::Notification(double time, void * sender, NotificationHandler * recipient, std::string info) :
	Event(time),
	simulator(nullptr),
	sender(sender),
	recipient(recipient),
	info(info) {}

Notification::~Notification() {}

void Notification::execute(Simulator * simulator) {
	this->simulator = simulator;
	if (recipient != 0){
		recipient->notify(this);
	}
}

void Notification::setSender(void * sender) {
	this->sender = sender;
}

void Notification::setRecipient(NotificationHandler * recipient) {
	this->recipient = recipient;
}

void Notification::setInfo(std::string info) {
	this->info = info;
}

void * Notification::getSender() const {
	return sender;
}

NotificationHandler * Notification::getRecipient() const {
	return recipient;
}

Simulator * Notification::getSimulator() const {
	return simulator;
}

std::string Notification::getInfo() const {
	return info;
}


string Notification::toString() const {
	stringstream ss;
	ss << "";//FIXME
	return ss.str();
}

/*std::ostream & operator<<(std::ostream & out, const Notification & message) {
	string source = ((message.sender == nullptr) ? "Null" : message.sender->getName());
	string destination = ((message.recipient == nullptr) ? "Null" : message.recipient->getName());
	out << (const Event&)message << '\n'
		<< std::setw(20) << std::left << "Sender: " <<  source << '\n'
		<< std::setw(20) << std::left << "Recipient: " <<  destination;

	return out;
}*/


