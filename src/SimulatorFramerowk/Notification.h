/*
 * Message.h
 *
 *  Created on: 18 nov 2016
 *      Author: cirod
 */

#pragma once

class Notification;

#include <ostream>
#include <string>
#include "Event.h"
#include "NotificationHandler.h"


class Notification : public Event {
public:
	Notification(double time);
	Notification(double time, NotificationHandler * recipient);
	Notification(double time, void * sender, NotificationHandler * recipient);
	Notification(double time, void * sender, NotificationHandler * recipient, std::string info);
	virtual ~Notification();

	void setSender(void * source);
	void setRecipient(NotificationHandler * recipient);
	void setInfo(std::string info);
	void * getSender() const;
	NotificationHandler * getRecipient() const;
	Simulator * getSimulator() const;
	std::string getInfo() const;

	void execute(Simulator * simulator);

	virtual std::string toString() const;
	friend std::ostream & operator<< (std::ostream & out, const Notification & message);

private:
	Notification(const Notification& message);
	Notification& operator=(const Notification& message);

	Simulator * simulator;
	void * sender;
	NotificationHandler * recipient;
	std::string info;

};


