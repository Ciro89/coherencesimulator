/*
 * EventsQueue.cpp
 *
 *  Created on: 18 nov 2016
 *      Author: cirod
 */

#include "EventsQueue.h"
#include <algorithm>


void EventsQueue::insert(Event * event) {
	this->push(event);
}

bool EventsQueue::remove(Event * event) {
	std::vector<Event*, std::allocator<Event*> >::iterator it = std::find(this->c.begin(), this->c.end(), event);
	if (it != this->c.end()) {
		this->c.erase(it);
		std::make_heap(this->c.begin(), this->c.end(), this->comp);
		return true;
	}
	else {
		return false;
	}
}

Event * EventsQueue::top() const {
	return std::priority_queue<Event*, std::vector<Event*, std::allocator<Event*> >, Event::EventComparator >::top();
}

void EventsQueue::pop() {
	std::priority_queue<Event*, std::vector<Event*, std::allocator<Event*> >, Event::EventComparator >::pop();
}

bool EventsQueue::empty() const {
	return std::priority_queue<Event*, std::vector<Event*, std::allocator<Event*> >, Event::EventComparator >::empty();
}

