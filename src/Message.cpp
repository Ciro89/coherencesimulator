#include "Message.h"


Message::Message(SystemComponent * recipient) :
	Message(nullptr, recipient) {}

Message::Message(SystemComponent * sender, SystemComponent * recipient) :
	sender(sender),
	recipient(recipient) {}


Message::~Message() {}



std::string Message::toString() const {
	return "";
}

SystemComponent * Message::getSender() const {
	return sender;
}

SystemComponent * Message::getRecipient() const {
	return recipient;
}

void Message::setSender(SystemComponent * sender) {
	this->sender = sender;
}

void Message::setRecipient(SystemComponent * recipient) {
	this->recipient = recipient;
}
