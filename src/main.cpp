/*
 * main.cpp
 *
 *  Created on: 04 dic 2016
 *      Author: cirod
 */

#include "MSIProtocol/MSICacheController.h"
#include "MSIProtocol/MSICore.h"
#include "InterconnectionNetwork.h"

using namespace std;

int main(int argc, char **argv) {
	Simulator simulator;
	unsigned int ncores = 2;
	double maxTime;

	cout << "#cores: ";
	cin >> ncores;
	cout << "Simulation time: ";
	cin >> maxTime;

	CoherenceDomain coherenceDomain(&simulator);

	InterconnectionNetwork requestNetwork("Request Network", &simulator);
	InterconnectionNetwork forwardedRequestNetwork("Forwarded Request Network", &simulator);
	InterconnectionNetwork responseNetwork("Response Network", &simulator);
	MSIDirectoryController directoryController("Directory Controller");
	MSICore ** cores = new MSICore*[ncores];

	directoryController.setReponseNetwork(&responseNetwork);
	directoryController.setForwardedRequestNetwork(&forwardedRequestNetwork);
	directoryController.setRequestNetwork(&requestNetwork);


	for (unsigned int i = 0; i < ncores; i++) {
		stringstream ss;
		ss << "Core" << i;
		cores[i] = new MSICore(ss.str(), &coherenceDomain);
		cores[i]->getCacheController()->setReponseNetwork(&responseNetwork);
		cores[i]->getCacheController()->setForwardedRequestNetwork(&forwardedRequestNetwork);
		cores[i]->getCacheController()->setRequestNetwork(&requestNetwork);
		cores[i]->getCacheController()->setDirectoryController(&directoryController);
		simulator.scheduleEvent(new Notification(0, nullptr, cores[i]));
	}


	cout <<"Inizio simulazione\n";
	simulator.run();


}
