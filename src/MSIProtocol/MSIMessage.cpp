/*
 * MSIMessage.cpp
 *
 *  Created on: 01 dic 2016
 *      Author: cirod
 */

#include <sstream>
#include "MSIMessage.h"

const char* const MSIMessage::TYPE_TO_STR[MSIMessage::MAX] = {
		"LOAD",
		"STORE",
		"REPLACEMENT",
		"GETS",
		"GETM",
		"PUTS",
		"PUTM",
		"FWD_GETS",
		"FWD_GETM",
		"INV",
		"PUT_ACK",
		"DATA",
		"INV_ACK"
};

MSIMessage::MSIMessage(SystemComponent * recipient, MessageType type, unsigned int address) :
	MSIMessage(nullptr, recipient, nullptr, type, address) {}

MSIMessage::MSIMessage(SystemComponent * sender, SystemComponent * recipient, SystemComponent * requestor, MessageType type, unsigned int address) :
	Message(sender, recipient),
	requestor(requestor),
	type(type),
	fromOwner(false),
	fromDirectory(false),
	ackCount(0),
	address(address) {}


SystemComponent * MSIMessage::getRequestor() const {
	return requestor;
}

MSIMessage::MessageType MSIMessage::getType() const {
	return type;
}

bool MSIMessage::isFromOwner() const {
	return fromOwner;
}

bool MSIMessage::isFromDirectory() const {
	return fromDirectory;
}

unsigned int MSIMessage::getAckCount() const {
	return ackCount;
}

unsigned int MSIMessage::getAddress() const {
	return address;
}



void MSIMessage::setRequestor(SystemComponent * requestor) {
	this->requestor = requestor;
}

void MSIMessage::setType(MessageType type) {
	this->type = type;
}

void MSIMessage::isFromOwner(bool isFromOwner) {
	this->fromOwner = isFromOwner;
}

void MSIMessage::isFromDirectory(bool isFromDirectory) {
	this->fromDirectory = isFromDirectory;
}

void MSIMessage::setAckCount(unsigned int ackCount) {
	this->ackCount = ackCount;
}

void MSIMessage::setAddress(unsigned int address) {
	this->address = address;
}

std::string MSIMessage::toString() const {
	std::stringstream ss;
	ss << *this;
	return ss.str();
}

std::ostream & operator<<(std::ostream & os, const MSIMessage & message) {
	os  << "Sender: " << message.sender->getName() << '\n'
		<< "Recipient: " << message.recipient->getName() << '\n'
		<< "Requestor: " << message.requestor->getName() << '\n'
		<< "Type: " << MSIMessage::TYPE_TO_STR[message.type] << '\n'
		<< "From owner: " << (message.fromOwner ? "True" : "False") << '\n'
		<< "From directory: " << (message.fromDirectory ? "True" : "False") << '\n'
		<< "AckCount: " << message.ackCount << '\n'
		<< "Address: " << message.address << '\n';
	return os;
}


