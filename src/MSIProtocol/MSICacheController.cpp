/*
 * MSICacheController.cpp
 *
 *  Created on: 03 dic 2016
 *      Author: cirod
 */

#include <initializer_list>
#include <cassert>
#include "MSICacheController.h"

const std::string MSICacheController::LOAD_INFO = "LOAD";
const std::string MSICacheController::STORE_INFO = "STORE";
const char * const MSICacheController::MSICacheBlock::STATE_TO_STR[MAX] = {"I", "S", "M", "IS_D", "IM_AD", "IM_A", "SM_AD", "SM_A", "MI_A", "SI_A", "II_A"};
//-----------------------------------------------------------------------------------------------------------------------------

MSICacheController::MSICacheBlock::MSICacheBlock() : state(MSICacheBlock::I) {}

MSICacheController::MSICacheBlock::BlockState MSICacheController::MSICacheBlock::getState() const {
	return state;
}

std::string  MSICacheController::MSICacheBlock::getData() const {
	return data;
}

std::string MSICacheController::MSICacheBlock::getSateAsString() const {
	return STATE_TO_STR[state];
}

void MSICacheController::MSICacheBlock::setState(BlockState state) {
	this->state = state;
}

void MSICacheController::MSICacheBlock::setData(std::string data) {
	this->data = data;
}

bool MSICacheController::MSICacheBlock::isInStableState() const {
	return (state == I || state == M || state == S);
}

//-----------------------------------------------------------------------------------------------------------------------------

MSICacheController::MSICacheController(std::string name, CoherenceDomain * coherenceDomain) :
	SystemComponent(name),
	coherenceDomain(coherenceDomain)
{
	core = nullptr;
	directoryController = nullptr;
	requestNetwork = nullptr;
	forwardedRequestNetwork = nullptr;
	responseNetwork = nullptr;
	lastCycle = -1;
	requestMessageCount = 0;
	responseMessageCount = 0;
	forwardedMessageCount = 0;
	storeMessageCount = 0;
	loadMessageCount = 0;
	ackCount = 0;
}

MSICacheController::~MSICacheController() {}

void MSICacheController::setDirectoryController(MSIDirectoryController * directoryController) {
	this->directoryController = directoryController;
}

void MSICacheController::setRequestNetwork(InterconnectionNetwork * network) {
	this->requestNetwork = network;
}

void MSICacheController::setForwardedRequestNetwork(InterconnectionNetwork * network) {
	this->forwardedRequestNetwork = network;
}

void MSICacheController::setReponseNetwork(InterconnectionNetwork * network) {
	this->responseNetwork = network;
}

void MSICacheController::setCore(MSICore * core) {
	this->core = core;
}



void MSICacheController::notify(Notification * notification) {

	MSIMessage * message = nullptr;
	MSIMessage * loadMessage = nullptr;
	MSIMessage * storeMessage = nullptr;
	MSIMessage * responseMessage = nullptr;
	MSIMessage * requestMessage = nullptr;
	MSIMessage * forwardedRequestMessage = nullptr;

	if (notification->getSender() == responseNetwork) {
		responseMessageCount++;
	} else if (notification->getSender() == forwardedRequestNetwork) {
		forwardedMessageCount++;
	} else if (notification->getSender() == requestNetwork) {
		requestMessageCount++;
	} else if (notification->getSender() == core && notification->getInfo() == LOAD_INFO) {
		loadMessageCount++;
	} else if (notification->getSender() == core && notification->getInfo() == STORE_INFO) {
		storeMessageCount++;
	}

	if (notification->getTime() != lastCycle) {
		loadMessage = dynamic_cast<MSIMessage*>((loadMessageCount ? core->topLoad() : nullptr));
		storeMessage = dynamic_cast<MSIMessage*>((storeMessageCount ? core->topStore() : nullptr));
		requestMessage = dynamic_cast<MSIMessage*>((requestMessageCount ? requestNetwork->getMessage(this) : nullptr));
		forwardedRequestMessage = dynamic_cast<MSIMessage*>((forwardedMessageCount ? forwardedRequestNetwork->getMessage(this) : nullptr));
		responseMessage = dynamic_cast<MSIMessage*>((responseMessageCount ? responseNetwork->getMessage(this) : nullptr));

		assert((loadMessage != nullptr) == (loadMessageCount > 0));
		assert((storeMessage != nullptr) == (storeMessageCount > 0));
		assert((requestMessage != nullptr) == (requestMessageCount > 0));
		assert((forwardedRequestMessage != nullptr) == (forwardedMessageCount > 0));
		assert((responseMessage != nullptr) == (responseMessageCount > 0));

		if (loadMessageCount && canHandleMessage(loadMessage)){
			message = loadMessage;
			core->popLoad();
			loadMessageCount--;
		} else if (responseMessageCount && canHandleMessage(responseMessage)) {
			message = responseMessage;
			responseNetwork->popMessage(this);
			responseMessageCount--;
		} else if (forwardedMessageCount && canHandleMessage(forwardedRequestMessage)){
			message = forwardedRequestMessage;
			forwardedRequestNetwork->popMessage(this);
			forwardedMessageCount--;
		} else if(requestMessageCount && canHandleMessage(requestMessage)) {
			message = requestMessage;
			requestNetwork->popMessage(this);
			requestMessageCount--;
		} else if (storeMessageCount && canHandleMessage(storeMessage)) {
			message = storeMessage;
			core->popStore();
			storeMessageCount--;
		}

		if (message != nullptr) {
			std::cout << "=========================================\n";
			std::cout << '\t' << name << " ( " << notification->getSimulator()->getTime() << " )\n\n";
			lastCycle = notification->getTime();
			std::cout << *message << '\n';
			handleMessage(message);
			delete message;
			std::cout << "=========================================\n";
		}
	}

	loadMessage = dynamic_cast<MSIMessage*>((loadMessageCount ? core->topLoad() : nullptr));
	storeMessage = dynamic_cast<MSIMessage*>((storeMessageCount ? core->topStore() : nullptr));
	requestMessage = dynamic_cast<MSIMessage*>((requestMessageCount ? requestNetwork->getMessage(this) : nullptr));
	forwardedRequestMessage = dynamic_cast<MSIMessage*>((forwardedMessageCount ? forwardedRequestNetwork->getMessage(this) : nullptr));
	responseMessage = dynamic_cast<MSIMessage*>((responseMessageCount ? responseNetwork->getMessage(this) : nullptr));

	if ((loadMessage && canHandleMessage(loadMessage)) ||
		(storeMessage && canHandleMessage(storeMessage)) ||
		(requestMessage && canHandleMessage(requestMessage)) ||
		(forwardedRequestMessage && canHandleMessage(forwardedRequestMessage)) ||
		(responseMessage && canHandleMessage(responseMessage)))
			notification->getSimulator()->scheduleEvent( new Notification (notification->getTime() + 1, this, this));

}


void MSICacheController::changeBlockState(unsigned int address, MSICacheBlock::BlockState newState) {
	MSICacheBlock::BlockState oldState = cache[address].getState();

	std::cout << "Block " << address << ": from " << cache[address].getSateAsString();
	cache[address].setState(newState);
	std::cout << " to " << cache[address].getSateAsString() << '\n';

	switch (oldState) {
		case MSICacheController::MSICacheBlock::M :
			coherenceDomain->decrementWriters(address);
			break;
		case MSICacheController::MSICacheBlock::S :
		case MSICacheController::MSICacheBlock::SM_AD :
		case MSICacheController::MSICacheBlock::SM_A :
			coherenceDomain->decrementReaders(address);
			break;
		default:
			break;
	}

	switch (newState) {
		case MSICacheController::MSICacheBlock::M :
			coherenceDomain->incrementWriters(address);
			break;
		case MSICacheController::MSICacheBlock::S :
		case MSICacheController::MSICacheBlock::SM_AD :
		case MSICacheController::MSICacheBlock::SM_A :
			coherenceDomain->incrementReaders(address);
			break;
		default:
			break;
	}

	coherenceDomain->check(address);


}




bool MSICacheController::canHandleMessage(MSIMessage * message) {
	//FIXME: Rendere generico
	//I, S, M, IS_D, IM_AD, IM_A, SM_AD, SM_A, MI_A, SI_A, II_A, MAX
	const static bool matrix[11][13] = {
		/*			LOAD		STORE		REPLECEMENT	GETS		GETM		PUTS		PUTM		FWD_GETS	FWD_GETM	INV			PUT_ACK		DATA		INV_ACK*/
		/*I		*/ {true	,	true	,	true	,	true	,	true	,	true	,	true	,	true	,	true	,	true	,	true	,	true	,	true	},
		/*S		*/ {true	,	true	,	true	,	true	,	true	,	true	,	true	,	true	,	true	,	true	,	true	,	true	,	true	},
		/*M		*/ {true	,	true	,	true	,	true	,	true	,	true	,	true	,	true	,	true	,	true	,	true	,	true	,	true	},
		/*IS_D	*/ {false	,	false	,	false	,	true	,	true	,	true	,	true	,	true	,	true	,	false	,	true	,	true	,	true	},
		/*IM_AD	*/ {false	,	false	,	false	,	true	,	true	,	true	,	true	,	false	,	false	,	true	,	true	,	true	,	true	},
		/*IM_A	*/ {false	,	false	,	false	,	true	,	true	,	true	,	true	,	false	,	false	,	true	,	true	,	true	,	true	},
		/*SM_AD	*/ {true	,	false	,	false	,	true	,	true	,	true	,	true	,	false	,	false	,	true	,	true	,	true	,	true	},
		/*SM_A	*/ {true	,	false	,	false	,	true	,	true	,	true	,	true	,	false	,	false	,	true	,	true	,	true	,	true	},
		/*MI_A	*/ {false	,	false	,	false	,	true	,	true	,	true	,	true	,	true	,	true	,	true	,	true	,	true	,	true	},
		/*SI_A	*/ {false	,	false	,	false	,	true	,	true	,	true	,	true	,	true	,	true	,	true	,	true	,	true	,	true	},
		/*II_A	*/ {false	,	false	,	false	,	true	,	true	,	true	,	true	,	true	,	true	,	true	,	true	,	true	,	true	},
	};

	MSIMessage::MessageType type = message->getType();
	MSICacheBlock::BlockState state = cache[message->getAddress()].getState();

	bool result = matrix[ state ][ type ];
	return result;
}

void MSICacheController::handleMessage(MSIMessage * message) {
	switch (cache[message->getAddress()].getState()) {
		case MSICacheBlock::I :
			state_I_Handler(message);
			break;
		case MSICacheBlock::S :
			state_S_Handler(message);
			break;
		case MSICacheBlock::M :
			state_M_Handler(message);
			break;
		case MSICacheBlock::IS_D:
			state_IS_D_Handler(message);
			break;
		case MSICacheBlock::IM_AD:
			state_IM_AD_Handler(message);
			break;
		case MSICacheBlock::IM_A:
			state_IM_A_Handler(message);
			break;
		case MSICacheBlock::SM_AD:
			state_SM_AD_Handler(message);
			break;
		case MSICacheBlock::SM_A:
			state_SM_A_Handler(message);
			break;
		case MSICacheBlock::MI_A:
			state_MI_A_Handler(message);
			break;
		case MSICacheBlock::SI_A:
			state_SI_A_Handler(message);
			break;
		case MSICacheBlock::II_A:
			state_II_A_Handler(message);
			break;
		default:
			assert(0);
			break;
		}
}

void MSICacheController::sendInvAckToRequestor(MSIMessage* message) {
	MSIMessage* response = new MSIMessage();
	response->setSender(this);
	response->setRecipient(message->getRequestor());
	response->setRequestor(message->getRequestor());
	response->setType(MSIMessage::INV_ACK);
	response->isFromOwner(false);
	response->isFromDirectory(false);
	response->setAckCount(0);
	response->setAddress(message->getAddress());
	responseNetwork->sendMessage(response);
}

void MSICacheController::sendDataToRequestor(MSIMessage* message) {
	MSIMessage* response = new MSIMessage();
	response->setSender(this);
	response->setRecipient(message->getRequestor());
	response->setRequestor(message->getRequestor());
	response->setType(MSIMessage::DATA);
	response->isFromOwner(true);
	response->isFromDirectory(false);
	response->setAckCount(0);
	response->setAddress(message->getAddress());
	responseNetwork->sendMessage(response);
}

void MSICacheController::sendDataToDirectoryController(MSIMessage* message) {
	MSIMessage* response = new MSIMessage();
	response->setSender(this);
	response->setRecipient(directoryController);
	response->setRequestor(message->getRequestor());
	response->setType(MSIMessage::DATA);
	response->isFromOwner(true);
	response->isFromDirectory(false);
	response->setAckCount(0);
	response->setAddress(message->getAddress());
	responseNetwork->sendMessage(response);
}


void MSICacheController::sendGetSToDirectoryController(MSIMessage* message) {
	MSIMessage* response = new MSIMessage();
	response->setSender(this);
	response->setRecipient(directoryController);
	response->setRequestor(this);
	response->setType(MSIMessage::GETS);
	response->isFromOwner(false);
	response->isFromDirectory(false);
	response->setAckCount(0);
	response->setAddress(message->getAddress());
	requestNetwork->sendMessage(response);
}

void MSICacheController::sendGetMToDirectoryController(MSIMessage* message) {
	MSIMessage* response = new MSIMessage();
	response->setSender(this);
	response->setRecipient(directoryController);
	response->setRequestor(this);
	response->setType(MSIMessage::GETM);
	response->isFromOwner(false);
	response->isFromDirectory(false);
	response->setAckCount(0);
	response->setAddress(message->getAddress());
	requestNetwork->sendMessage(response);
}

void MSICacheController::state_I_Handler(MSIMessage * message) {
	switch (message->getType()) {
		case MSIMessage::LOAD :
			sendGetSToDirectoryController(message);
			changeBlockState(message->getAddress(), MSICacheBlock::IS_D);
			break;
		case MSIMessage::STORE :
			sendGetMToDirectoryController(message);
			changeBlockState(message->getAddress(), MSICacheBlock::IM_AD);
			break;;
		default:
			//-------------------------------------------------------------------------------
			assert(0);
			//-------------------------------------------------------------------------------
			break;
	}
}

void MSICacheController::sendPutSToDirectoryController(MSIMessage* message) {
	MSIMessage* response = new MSIMessage();
	response->setSender(this);
	response->setRecipient(directoryController);
	response->setRequestor(this);
	response->setType(MSIMessage::PUTS);
	response->isFromOwner(false);
	response->isFromDirectory(false);
	response->setAckCount(0);
	response->setAddress(message->getAddress());
	requestNetwork->sendMessage(response);
}

void MSICacheController::state_S_Handler(MSIMessage * message) {
	switch (message->getType()) {
		case MSIMessage::LOAD :
			//TODO: Hit
			break;
		case MSIMessage::STORE :
			sendGetMToDirectoryController(message);
			changeBlockState(message->getAddress(), MSICacheBlock::SM_AD);
			break;
		case MSIMessage::REPLACEMENT :
			sendPutSToDirectoryController(message);
			changeBlockState(message->getAddress(), MSICacheBlock::SI_A);
			break;
		case MSIMessage::INV :
			//-------------------------------------------------------------------------------
			sendInvAckToRequestor(message);
			changeBlockState(message->getAddress(), MSICacheBlock::I);
			//-------------------------------------------------------------------------------
			break;
		default :
			//-------------------------------------------------------------------------------
			assert(0);
			//-------------------------------------------------------------------------------
			break;
	}
}

void MSICacheController::sendPutMToDitectoryController(MSIMessage* message) {
	MSIMessage* response = new MSIMessage();
	response->setSender(this);
	response->setRecipient(directoryController);
	response->setRequestor(this);
	response->setType(MSIMessage::PUTM);
	response->isFromOwner(false);
	response->isFromDirectory(false);
	response->setAckCount(0);
	response->setAddress(message->getAddress());
	requestNetwork->sendMessage(response);
}

void MSICacheController::state_M_Handler(MSIMessage * message) {
	switch (message->getType()) {
		case MSIMessage::LOAD :
			//TODO: Hit
			break;
		case MSIMessage::STORE :
			//TODO: Hit
			break;
		case MSIMessage::REPLACEMENT :
			sendPutMToDitectoryController(message);
			changeBlockState(message->getAddress(), MSICacheBlock::MI_A);
			break;
		case MSIMessage::FWD_GETS :
			//-------------------------------------------------------------------------------
			sendDataToRequestor(message);
			sendDataToDirectoryController(message);
			changeBlockState(message->getAddress(), MSICacheBlock::S);
			//-------------------------------------------------------------------------------
			break;
		case MSIMessage::FWD_GETM :
			//-------------------------------------------------------------------------------
			sendDataToRequestor(message);
			changeBlockState(message->getAddress(), MSICacheBlock::I);
			//-------------------------------------------------------------------------------
			break;
		default:
			//-------------------------------------------------------------------------------
			assert(0);
			//-------------------------------------------------------------------------------
			break;
	}
}

void MSICacheController::state_IS_D_Handler(MSIMessage * message) {
	switch (message->getType()) {
		case MSIMessage::LOAD :
		case MSIMessage::STORE :
		case MSIMessage::REPLACEMENT :
		case MSIMessage::INV :
			assert(0);
			break;
		case MSIMessage::DATA :
			//-------------------------------------------------------------------------------
			if (message->isFromDirectory()) {
				//assert(message->getAckCount() == 0);
				changeBlockState(message->getAddress(), MSICacheBlock::S);
			} else if (message->isFromOwner()) {
				changeBlockState(message->getAddress(), MSICacheBlock::S);
			}
			//-------------------------------------------------------------------------------
			break;
		default:
			//-------------------------------------------------------------------------------
			assert(0);
			//-------------------------------------------------------------------------------
			break;

	}
}

void MSICacheController::state_IM_AD_Handler(MSIMessage * message) {
	switch (message->getType()) {
		case MSIMessage::LOAD :
		case MSIMessage::STORE :
		case MSIMessage::REPLACEMENT :
		case MSIMessage::FWD_GETS :
		case MSIMessage::FWD_GETM :
			assert(0);
			break;
		case MSIMessage::DATA :
			//-------------------------------------------------------------------------------
			if (message->isFromDirectory()) {
				ackCount = message->getAckCount();
				if (ackCount == 0)
					changeBlockState(message->getAddress(), MSICacheBlock::M);
				else
					changeBlockState(message->getAddress(), MSICacheBlock::IM_A);
			} else {
				assert(message->isFromOwner());
				changeBlockState(message->getAddress(), MSICacheBlock::M);
			}
			//-------------------------------------------------------------------------------
			break;
		case MSIMessage::INV_ACK :
			//-------------------------------------------------------------------------------
			ackCount--;
			assert(ackCount > 0);
			//-------------------------------------------------------------------------------
			break;
		default:
			//-------------------------------------------------------------------------------
			assert(0);
			//-------------------------------------------------------------------------------
			break;

	}
}


void MSICacheController::state_IM_A_Handler(MSIMessage * message) {
	switch (message->getType()) {
		case MSIMessage::LOAD :
		case MSIMessage::STORE :
		case MSIMessage::REPLACEMENT :
		case MSIMessage::FWD_GETS :
		case MSIMessage::FWD_GETM :
			assert(0);
			break;
		case MSIMessage::INV_ACK :
			//-------------------------------------------------------------------------------
			ackCount--;
			if (ackCount == 0)
				changeBlockState(message->getAddress(), MSICacheBlock::M);
			//-------------------------------------------------------------------------------
			break;
		default:
			//-------------------------------------------------------------------------------
			assert(0);
			//-------------------------------------------------------------------------------
			break;

	}
}

void MSICacheController::state_SM_AD_Handler(MSIMessage * message) {
	switch (message->getType()) {
		case MSIMessage::LOAD :
			//TODO: hit
			break;
		case MSIMessage::STORE :
		case MSIMessage::REPLACEMENT :
		case MSIMessage::FWD_GETS :
		case MSIMessage::FWD_GETM :
			assert(0);
			break;
		case MSIMessage::INV :
			//-------------------------------------------------------------------------------
			sendInvAckToRequestor(message);
			changeBlockState(message->getAddress(), MSICacheBlock::IM_AD);
			//-------------------------------------------------------------------------------
			break;
		case MSIMessage::DATA :
			//-------------------------------------------------------------------------------
			if (message->isFromDirectory()) {
				ackCount = message->getAckCount() - 1; //TODO: Importante
				if (ackCount == 0)
					changeBlockState(message->getAddress(), MSICacheBlock::M);
				else
					changeBlockState(message->getAddress(), MSICacheBlock::SM_A);
			} else {
				assert(message->isFromOwner());
				changeBlockState(message->getAddress(), MSICacheBlock::M);
			}
			//-------------------------------------------------------------------------------
			break;
		case MSIMessage::INV_ACK :
			//-------------------------------------------------------------------------------
			ackCount--;
			assert(ackCount != 0);
			//-------------------------------------------------------------------------------
			break;
		default:
			//-------------------------------------------------------------------------------
			assert(0);
			//-------------------------------------------------------------------------------
			break;

	}
}

void MSICacheController::state_SM_A_Handler(MSIMessage * message) {
	switch (message->getType()) {
		case MSIMessage::LOAD :
			//TODO:Hit
			break;
		case MSIMessage::STORE :
		case MSIMessage::REPLACEMENT :
		case MSIMessage::FWD_GETS :
		case MSIMessage::FWD_GETM :
			assert(0);
			break;
		case MSIMessage::INV_ACK :
			//-------------------------------------------------------------------------------
			ackCount--;
			if (ackCount == 0)
				changeBlockState(message->getAddress(), MSICacheBlock::M);
			//-------------------------------------------------------------------------------
			break;
		default:
			//-------------------------------------------------------------------------------
			assert(0);
			//-------------------------------------------------------------------------------
			break;

	}
}

void MSICacheController::state_MI_A_Handler(MSIMessage * message) {
	switch (message->getType()) {
		case MSIMessage::LOAD :
			//TODO: Hit
			break;
		case MSIMessage::STORE :
			//TODO: Hit
			break;
		case MSIMessage::REPLACEMENT :
			sendPutMToDitectoryController(message);
			changeBlockState(message->getAddress(), MSICacheBlock::MI_A);
			break;
		case MSIMessage::FWD_GETS :
			//-------------------------------------------------------------------------------
			sendDataToRequestor(message);
			sendDataToDirectoryController(message);
			changeBlockState(message->getAddress(), MSICacheBlock::SI_A);
			//-------------------------------------------------------------------------------
			break;
		case MSIMessage::FWD_GETM :
			//-------------------------------------------------------------------------------
			sendDataToRequestor(message);
			changeBlockState(message->getAddress(), MSICacheBlock::II_A);
			//-------------------------------------------------------------------------------
			break;
		case MSIMessage::PUT_ACK :
			//-------------------------------------------------------------------------------
			changeBlockState(message->getAddress(), MSICacheBlock::I);
			//-------------------------------------------------------------------------------
			break;
		default:
			assert(0);
			break;

	}
}


void MSICacheController::state_SI_A_Handler(MSIMessage * message) {
	switch (message->getType()) {
		case MSIMessage::LOAD :
		case MSIMessage::STORE :
		case MSIMessage::REPLACEMENT :
			assert(0);
			break;
		case MSIMessage::INV :
			//-------------------------------------------------------------------------------
			sendInvAckToRequestor(message);
			changeBlockState(message->getAddress(), MSICacheBlock::II_A);
			//-------------------------------------------------------------------------------
			break;
		case MSIMessage::PUT_ACK :
			//-------------------------------------------------------------------------------
			changeBlockState(message->getAddress(), MSICacheBlock::I);
			//-------------------------------------------------------------------------------
			break;
		default:
			assert(0);
			break;

	}
}

void MSICacheController::state_II_A_Handler(MSIMessage * message) {
	switch (message->getType()) {
		case MSIMessage::LOAD :
		case MSIMessage::STORE :
		case MSIMessage::REPLACEMENT :
			assert(0);
			break;
		case MSIMessage::PUT_ACK :
			//-------------------------------------------------------------------------------
			changeBlockState(message->getAddress(), MSICacheBlock::I);
			//-------------------------------------------------------------------------------
			break;
		default:
			assert(0);
			break;

	}
}



