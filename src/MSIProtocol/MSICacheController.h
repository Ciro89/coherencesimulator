/*
 * MSICacheController.h
 *
 *  Created on: 03 dic 2016
 *      Author: cirod
 */

#pragma once

class MSICacheController;

#include <map>
#include <string>

#include "../SystemComponent.h"
#include "../InterconnectionNetwork.h"
#include "../CoherenceDomain.h"

#include "MSIMessage.h"
#include "MSIDirectoryController.h"
#include "MSICore.h"

class MSICacheController : public SystemComponent {
private:

	class MSICacheBlock {
	public:

		typedef enum {
			I, S, M, IS_D, IM_AD, IM_A, SM_AD, SM_A, MI_A, SI_A, II_A, MAX
		} BlockState;

		MSICacheBlock();

		BlockState getState() const;
		std::string  getData() const;
		std::string getSateAsString() const;
		void setState(BlockState state);
		void setData(std::string data);
		bool isInStableState() const;

	private:
		BlockState state;
		std::string data;

		static const char * const STATE_TO_STR[];
	};

	CoherenceDomain * coherenceDomain;
	MSICore * core;
	MSIDirectoryController * directoryController;
	InterconnectionNetwork * requestNetwork;
	InterconnectionNetwork * forwardedRequestNetwork;
	InterconnectionNetwork * responseNetwork;

	double lastCycle;

	unsigned int requestMessageCount;
	unsigned int responseMessageCount;
	unsigned int forwardedMessageCount;
	unsigned int loadMessageCount;
	unsigned int storeMessageCount;
	unsigned int ackCount;


	std::map<unsigned int, MSICacheBlock> cache;

	bool canHandleMessage(MSIMessage * message);
	void handleMessage(MSIMessage * message);


	void state_I_Handler(MSIMessage * message);
	void state_S_Handler(MSIMessage * message);
	void state_M_Handler(MSIMessage * message);
	void state_IS_D_Handler(MSIMessage * message);
	void state_IM_AD_Handler(MSIMessage * message);
	void state_IM_A_Handler(MSIMessage * message);
	void state_SM_AD_Handler(MSIMessage * message);
	void state_SM_A_Handler(MSIMessage * message);
	void state_MI_A_Handler(MSIMessage * message);
	void state_SI_A_Handler(MSIMessage * message);
	void state_II_A_Handler(MSIMessage * message);


	void sendInvAckToRequestor(MSIMessage* message);
	void changeBlockState(unsigned int address, MSICacheBlock::BlockState newState);
	void sendDataToRequestor(MSIMessage* message);
	void sendDataToDirectoryController(MSIMessage* message);
	void sendGetSToDirectoryController(MSIMessage* message);
	void sendGetMToDirectoryController(MSIMessage* message);
	void sendPutSToDirectoryController(MSIMessage* message);
	void sendPutMToDitectoryController(MSIMessage* message);


public:
	MSICacheController(std::string name, CoherenceDomain * coherenceDomain = nullptr);
	virtual ~MSICacheController();

	void setDirectoryController(MSIDirectoryController * directoryController);
	void setRequestNetwork(InterconnectionNetwork * network);
	void setForwardedRequestNetwork(InterconnectionNetwork * network);
	void setReponseNetwork(InterconnectionNetwork * network);
	void setCore(MSICore * core);


	SystemComponent * getDirectoryController() const;
	InterconnectionNetwork * getRequestNetwork() const;
	InterconnectionNetwork * getForwardedRequestNetwork() const;
	InterconnectionNetwork * getResposeNetwork() const;

	virtual void notify(Notification * notification);

	static const std::string LOAD_INFO;
	static const std::string STORE_INFO;

};
