/*
 * MSIDirectoryController.h
 *
 *  Created on: 01 dic 2016
 *      Author: cirod
 */
#pragma once

#include <map>
#include <set>
#include <string>
#include "../SystemComponent.h"
#include "../InterconnectionNetwork.h"
#include "../Message.h"
#include "MSIMessage.h"


class MSIDirectoryController : public SystemComponent {
private:

	class MSIDirectoryEntry {
		public:

			typedef enum  {
				I, S, M, S_D, MAX
			} EntryState;

			MSIDirectoryEntry(EntryState state = I, SystemComponent * owner = nullptr);
			void addSharer(SystemComponent * sharer);
			void removeSharer(SystemComponent * sharer);
			void cleanSharers();
			bool isInStableState() const;

			int getSharerCount() const;
			EntryState getState() const;
			SystemComponent * getOwner() const;
			std::set<SystemComponent*> getSharerSet() const;
			std::string getStateAsString() const;

			void setState(EntryState state);
			void setOwner(SystemComponent * owner);

		private:
			EntryState state;
			SystemComponent * owner;
			std::set<SystemComponent *> sharers;
			static const char * const STATE_TO_STR[];
		} ;

	void handleMessage(MSIMessage * message);
	void state_I_Handler(MSIMessage * message);
	void state_S_Handler(MSIMessage * message);
	void state_M_Handler(MSIMessage * message);
	void state_S_D_Handler(MSIMessage * message);
	void sendDataToRequestor(MSIMessage* message);
	void sendPutAckToRequestor(MSIMessage* message);
	void sendInvToSharers(MSIMessage* message);
	void sendFwdGetSToOwner(MSIMessage* message);
	void sendFwdGetMToOwner(MSIMessage* message);

	void changeEntryState(unsigned int address, MSIDirectoryEntry::EntryState newState);

	bool canHandleMessage(MSIMessage * message);

	InterconnectionNetwork * requestNetwork;
	InterconnectionNetwork * forwardedRequestNetwork;
	InterconnectionNetwork * responseNetwork;

	std::map<unsigned int, MSIDirectoryEntry> directory;
	std::map<unsigned int, std::string> memory;
	double lastCycle;

	unsigned int requestMessageCount;
	unsigned int responseMessageCount;
	unsigned int forwardedMessageCount;

public:

	MSIDirectoryController(std::string name);
	virtual ~MSIDirectoryController();

	void setRequestNetwork(InterconnectionNetwork * network);
	void setForwardedRequestNetwork(InterconnectionNetwork * network);
	void setReponseNetwork(InterconnectionNetwork * network);
	InterconnectionNetwork * getRequestNetwork();
	InterconnectionNetwork * getForwardedRequestNetwork();
	InterconnectionNetwork * getReponseNetwork();

	virtual void notify(Notification * notification);
};

