/*
 * Core.cpp
 *
 *  Created on: 03 dic 2016
 *      Author: cirod
 */

#include <cassert>
#include "../SimulatorFramerowk/Simulator.h"
#include "MSICore.h"

int MSICore::storeProbability = 50;
int MSICore::loadProbability = 50;
int MSICore::evictProbability = 0;
int MSICore::requestsInterarrivaleTime = 2;

void MSICore::setRequestsProbability(int storeProbability, int loadProbability, int evictProbability) {
	//FIXME: Fare controlli
	MSICore::storeProbability = storeProbability;
	MSICore::loadProbability = loadProbability;
	MSICore::evictProbability = evictProbability;
}

void MSICore::setRequestsInterarrivaleTime(int time) {
	MSICore::requestsInterarrivaleTime = time;
}


MSICore::MSICore(std::string name, CoherenceDomain * coherenceDomain) :
	SystemComponent(name),
	loadQueue(),
	storeQueue(),
	requestTimeDistribution(1, 100),
	requestTypeDistribution(0, 99),
	requestAddressDistribution(0, 0),
	requestTimeGenerator(),
	requestTypeGenerator(),
	requestAddressGenerator()
{
	cacheController = new MSICacheController(name + ".CacheController", coherenceDomain);
	cacheController->setCore(this);
}

MSICore::~MSICore() {
	delete cacheController;
}

MSIMessage * MSICore::topLoad() const {
	return loadQueue.front();

}

MSIMessage * MSICore::topStore() const {
	return storeQueue.front();
}

void MSICore::popLoad() {
	loadQueue.pop();
}

void MSICore::popStore() {
	storeQueue.pop();
}

void MSICore::notify(Notification * notification) {
	MSIMessage * message = new MSIMessage();
	message->setRecipient(cacheController);
	message->setSender(this);
	message->setRequestor(this);

	double nextTime = notification->getTime() + requestTimeDistribution(requestTimeGenerator);

	int randomType = requestTypeDistribution(requestTypeGenerator);

	if  (0 <= randomType && randomType < loadProbability) {
		message->setType(MSIMessage::LOAD);
		loadQueue.push(message);
		notification->getSimulator()->scheduleEvent(new Notification(nextTime, this, cacheController, MSICacheController::LOAD_INFO));
	} else if (loadProbability <= randomType && randomType < loadProbability + storeProbability) {
		message->setType(MSIMessage::STORE);
		storeQueue.push(message);
		notification->getSimulator()->scheduleEvent(new Notification(nextTime, this, cacheController, MSICacheController::STORE_INFO));
	} else if (loadProbability + storeProbability <= randomType && randomType < loadProbability + storeProbability + evictProbability) {
		message->setType(MSIMessage::REPLACEMENT);
		//FIXME: Insert in evict queue
		//notification->getSimulator()->scheduleEvent(new Notification(nextTime, this, cacheController, MSICacheController::EVICT_INFO));
	}

	notification->getSimulator()->scheduleEvent(new Notification(nextTime, this, this));
}

MSICacheController * MSICore::getCacheController() const {
	return cacheController;
}
