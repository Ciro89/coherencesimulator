/*
 * Core.h
 *
 *  Created on: 03 dic 2016
 *      Author: cirod
 */

#pragma once

class MSICore;

#include <queue>
#include <random>

#include "../SystemComponent.h"
#include "../Message.h"

#include "MSIMessage.h"
#include "MSICacheController.h"


class MSICore : public SystemComponent {
public:
	MSICore(std::string name, CoherenceDomain * coherenceDomain = nullptr);
	virtual ~MSICore();

	MSIMessage * topLoad() const;
	MSIMessage * topStore() const;

	void popLoad();
	void popStore();

	virtual void notify(Notification * notification);

	MSICacheController * getCacheController() const;

	static void setRequestsProbability(int storeProbability, int loadProbability, int evictProbability);
	static void setRequestsInterarrivaleTime(int time);

private:
	MSICacheController * cacheController;

	static int storeProbability;
	static int loadProbability;
	static int evictProbability;
	static int requestsInterarrivaleTime;

	std::queue <MSIMessage *> loadQueue;
	std::queue <MSIMessage *> storeQueue;

	std::uniform_int_distribution<int> requestTimeDistribution;
	std::uniform_int_distribution<int> requestTypeDistribution;
	std::uniform_int_distribution<unsigned int> requestAddressDistribution;

	std::random_device requestTimeGenerator;
	std::random_device requestTypeGenerator;
	std::random_device requestAddressGenerator;

};


