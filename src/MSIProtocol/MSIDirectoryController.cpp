/*
 * MSIDirectoryController.cpp
 *
 *  Created on: 01 dic 2016
 *      Author: cirod
 */

#include <cassert>
#include "MSIDirectoryController.h"

const char * const MSIDirectoryController::MSIDirectoryEntry::STATE_TO_STR[MAX] = {"I", "S", "M", "S_D"};

MSIDirectoryController::MSIDirectoryEntry::MSIDirectoryEntry(EntryState state, SystemComponent * owner) :
	state(state),
	owner(owner),
	sharers() {}

void MSIDirectoryController::MSIDirectoryEntry::addSharer(SystemComponent * sharer) {
	sharers.insert(sharer);
}

void MSIDirectoryController::MSIDirectoryEntry::removeSharer(SystemComponent * sharer) {
	sharers.erase(sharer);
}

int MSIDirectoryController::MSIDirectoryEntry::getSharerCount() const {
	return sharers.size();
}

MSIDirectoryController::MSIDirectoryEntry::EntryState MSIDirectoryController::MSIDirectoryEntry::getState() const {
	return state;
}

SystemComponent * MSIDirectoryController::MSIDirectoryEntry::getOwner() const {
	return owner;
}

std::set<SystemComponent*> MSIDirectoryController::MSIDirectoryEntry::getSharerSet() const {
	return sharers;
}

std::string MSIDirectoryController::MSIDirectoryEntry::getStateAsString() const {
	return STATE_TO_STR[state];
}

void MSIDirectoryController::MSIDirectoryEntry::setState(EntryState state) {
	this->state = state;
}

void MSIDirectoryController::MSIDirectoryEntry::setOwner(SystemComponent * owner) {
	this->owner = owner;
}

void MSIDirectoryController::MSIDirectoryEntry::cleanSharers() {
	sharers.clear();
}

bool MSIDirectoryController::MSIDirectoryEntry::isInStableState() const {
	return (state == I || state == M || state == S);
}
//-------------------------------------------------------------------------------------------------------------------------------


MSIDirectoryController::MSIDirectoryController(std::string name) : SystemComponent(name) {
	requestNetwork = nullptr;
	forwardedRequestNetwork = nullptr;
	responseNetwork = nullptr;

	lastCycle = -1;

	requestMessageCount = 0;
	responseMessageCount = 0;
	forwardedMessageCount = 0;
}

MSIDirectoryController::~MSIDirectoryController() {}

void MSIDirectoryController::setRequestNetwork(InterconnectionNetwork * network) {
	requestNetwork = network;
}

void MSIDirectoryController::setForwardedRequestNetwork(InterconnectionNetwork * network) {
	forwardedRequestNetwork = network;
}

void MSIDirectoryController::setReponseNetwork(InterconnectionNetwork * network) {
	responseNetwork = network;
}

InterconnectionNetwork * MSIDirectoryController::getRequestNetwork() {
	return requestNetwork;
}

InterconnectionNetwork * MSIDirectoryController::getForwardedRequestNetwork() {
	return forwardedRequestNetwork;
}

InterconnectionNetwork * MSIDirectoryController::getReponseNetwork() {
	return responseNetwork;
}


void MSIDirectoryController::notify(Notification * notification) {

	MSIMessage * message = nullptr;
	MSIMessage * responseMessage = nullptr;
	MSIMessage * requestMessage = nullptr;
	MSIMessage * forwardedRequestMessage = nullptr;



	if (notification->getSender() == responseNetwork) {
		responseMessageCount++;
	} else if (notification->getSender() == forwardedRequestNetwork) {
		forwardedMessageCount++;
	} else if (notification->getSender() == requestNetwork) {
		requestMessageCount++;
	}

	if (notification->getTime() != lastCycle) {
		requestMessage = dynamic_cast<MSIMessage*>((requestMessageCount ? requestNetwork->getMessage(this) : nullptr));
		forwardedRequestMessage = dynamic_cast<MSIMessage*>((forwardedMessageCount ? forwardedRequestNetwork->getMessage(this) : nullptr));
		responseMessage = dynamic_cast<MSIMessage*>((responseMessageCount ? responseNetwork->getMessage(this) : nullptr));

		assert((requestMessage != nullptr) == (requestMessageCount > 0));
		assert((forwardedRequestMessage != nullptr) == (forwardedMessageCount > 0));
		assert((responseMessage != nullptr) == (responseMessageCount > 0));

		if (responseMessageCount && canHandleMessage(responseMessage)) {
			message = responseMessage;
			responseNetwork->popMessage(this);
			responseMessageCount--;
		} else if (forwardedMessageCount && canHandleMessage(forwardedRequestMessage)){
			message = forwardedRequestMessage;
			forwardedRequestNetwork->popMessage(this);
			forwardedMessageCount--;
		} else if(requestMessageCount && canHandleMessage(requestMessage)) {
			message = requestMessage;
			requestNetwork->popMessage(this);
			requestMessageCount--;
		}

		if (message != nullptr) {
			std::cout << "=========================================\n";
			std::cout << '\t' << name << " ( " << notification->getSimulator()->getTime() << " )\n\n";
			lastCycle = notification->getTime();
			std::cout << *message << '\n';
			handleMessage(message);
			delete message;
			std::cout << "=========================================\n";
		}
	}

	requestMessage = dynamic_cast<MSIMessage*>((requestMessageCount ? requestNetwork->getMessage(this) : nullptr));
	forwardedRequestMessage = dynamic_cast<MSIMessage*>((forwardedMessageCount ? forwardedRequestNetwork->getMessage(this) : nullptr));
	responseMessage = dynamic_cast<MSIMessage*>((responseMessageCount ? responseNetwork->getMessage(this) : nullptr));


	//TODO: Se la coda prioritaria dell'interconnessione non � stabile possono verificarsi dei problemi.
	if ((requestMessage && canHandleMessage(requestMessage)) ||
		(forwardedRequestMessage && canHandleMessage(forwardedRequestMessage)) ||
		(responseMessage && canHandleMessage(responseMessage)))
			notification->getSimulator()->scheduleEvent( new Notification (notification->getTime() + 1, this, this));

}

bool MSIDirectoryController::canHandleMessage(MSIMessage * message) {
	//FIXME: Rendere generico
	//I, S, M, IS_D, IM_AD, IM_A, SM_AD, SM_A, MI_A, SI_A, II_A, MAX
	const static bool matrix[11][13] = {
		/*			LOAD		STORE		REPLECEMENT	GETS		GETM		PUTS		PUTM		FWD_GETS	FWD_GETM	INV			PUT_ACK		DATA		INV_ACK*/
		/*I		*/ {true	,	true	,	true	,	true	,	true	,	true	,	true	,	true	,	true	,	true	,	true	,	true	,	true	},
		/*M		*/ {true	,	true	,	true	,	true	,	true	,	true	,	true	,	true	,	true	,	true	,	true	,	true	,	true	},
		/*S		*/ {true	,	true	,	true	,	true	,	true	,	true	,	true	,	true	,	true	,	true	,	true	,	true	,	true	},
		/*S_D	*/ {true	,	true	,	true	,	false	,	false	,	true	,	true	,	true	,	true	,	true	,	true	,	true	,	true	}
	};

	MSIMessage::MessageType type = message->getType();
	MSIDirectoryEntry::EntryState state = directory[message->getAddress()].getState();

	bool result = matrix[ state ][ type ];
	return result;
}


void MSIDirectoryController::handleMessage(MSIMessage * message) {
	switch (directory[message->getAddress()].getState()) {
	case MSIDirectoryEntry::I :
		state_I_Handler(message);
		break;
	case MSIDirectoryEntry::M :
		state_M_Handler(message);
		break;
	case MSIDirectoryEntry::S :
		state_S_Handler(message);
		break;
	case MSIDirectoryEntry::S_D:
		state_S_D_Handler(message);
		break;
	default:
		assert(0);
		break;
	}
}

//--------------------------------------------------------------------------------------

void MSIDirectoryController::sendDataToRequestor(MSIMessage* message) {
	MSIMessage * response = new MSIMessage();
	response->setSender(this);
	response->setRecipient(message->getRequestor());
	response->setRequestor(message->getRequestor());
	response->setType(MSIMessage::DATA);
	response->isFromOwner(true);
	response->isFromDirectory(true);
	response->setAckCount(directory[message->getAddress()].getSharerCount());
	response->setAddress(message->getAddress());
	responseNetwork->sendMessage(response);
}

void MSIDirectoryController::sendPutAckToRequestor(MSIMessage* message) {
	MSIMessage* response = new MSIMessage();
	response->setSender(this);
	response->setRecipient(message->getRequestor());
	response->setRequestor(message->getRequestor());
	response->setType(MSIMessage::PUT_ACK);
	response->isFromOwner(false);
	response->isFromDirectory(true);
	response->setAckCount(directory[message->getAddress()].getSharerCount());
	response->setAddress(message->getAddress());
	forwardedRequestNetwork->sendMessage(response);
}

void MSIDirectoryController::sendInvToSharers(MSIMessage* message) {
	std::set<SystemComponent *> sharers = directory[message->getAddress()].getSharerSet();
	for (std::set<SystemComponent *>::iterator it = sharers.begin(); it != sharers.end(); it++) {
		if (*it != message->getRequestor()) { //TODO: Importante
			MSIMessage* response = new MSIMessage();
			response->setSender(this);
			response->setRecipient(*it);
			response->setRequestor(message->getRequestor());
			response->setType(MSIMessage::INV);
			response->isFromOwner(false);
			response->isFromDirectory(true);
			response->setAckCount(directory[message->getAddress()].getSharerCount());
			response->setAddress(message->getAddress());
			forwardedRequestNetwork->sendMessage(response);
		}
	}
}

void MSIDirectoryController::sendFwdGetSToOwner(MSIMessage* message) {
	MSIMessage* response = new MSIMessage();
	response->setSender(this);
	response->setRecipient(directory[message->getAddress()].getOwner());
	response->setRequestor(message->getRequestor());
	response->setType(MSIMessage::FWD_GETS);
	response->isFromOwner(false);
	response->isFromDirectory(true);
	response->setAckCount(directory[message->getAddress()].getSharerCount());
	response->setAddress(message->getAddress());
	forwardedRequestNetwork->sendMessage(response);
}

void MSIDirectoryController::sendFwdGetMToOwner(MSIMessage* message) {
	MSIMessage* response = new MSIMessage();
	response->setSender(this);
	response->setRecipient(directory[message->getAddress()].getOwner());
	response->setRequestor(message->getRequestor());
	response->setType(MSIMessage::FWD_GETM);
	response->isFromOwner(false);
	response->isFromDirectory(true);
	response->setAckCount(directory[message->getAddress()].getSharerCount());
	response->setAddress(message->getAddress());
	forwardedRequestNetwork->sendMessage(response);
}


void MSIDirectoryController::changeEntryState(unsigned int address, MSIDirectoryEntry::EntryState newState) {
	std::cout << "Entry " << address << ": from " << directory[address].getStateAsString();
	directory[address].setState(newState);
	std::cout << " to  " << directory[address].getStateAsString() << '\n';
}

//--------------------------------------------------------------------------------------

void MSIDirectoryController::state_I_Handler(MSIMessage * message) {
	MSIDirectoryEntry & directoryEntry = directory[message->getAddress()];
	switch (message->getType()) {
	case MSIMessage::GETS :
		sendDataToRequestor(message);
		directoryEntry.addSharer(message->getRequestor());
		changeEntryState(message->getAddress(), MSIDirectoryEntry::S);
		break;
	case MSIMessage::GETM :
		sendDataToRequestor(message);
		directoryEntry.setOwner(message->getRequestor());
		changeEntryState(message->getAddress(), MSIDirectoryEntry::M);
		break;
	case MSIMessage::PUTS :
		sendPutAckToRequestor(message);
		break;
	case MSIMessage::PUTM :
		if (message->getSender() != directoryEntry.getOwner())
			sendPutAckToRequestor(message);
		else
			assert(0);
		break;
	default :
		assert(0);
	}
}

void MSIDirectoryController::state_S_Handler(MSIMessage * message) {
	MSIDirectoryEntry & directoryEntry = directory[message->getAddress()];
	switch (message->getType()) {
	case MSIMessage::GETS :
		sendDataToRequestor(message);
		directoryEntry.addSharer(message->getRequestor());
		break;
	case MSIMessage::GETM :
		sendDataToRequestor(message);
		sendInvToSharers(message);
		directoryEntry.cleanSharers();
		directoryEntry.setOwner(message->getRequestor());
		changeEntryState(message->getAddress(), MSIDirectoryEntry::M);
		break;
	case MSIMessage::PUTS :
		directoryEntry.removeSharer(message->getRequestor());
		sendPutAckToRequestor(message);
		if (directoryEntry.getSharerCount() == 0)
			changeEntryState(message->getAddress(), MSIDirectoryEntry::I);
		break;
	case MSIMessage::PUTM :
		if (!message->isFromOwner()) {
			directoryEntry.removeSharer(message->getRequestor());
			sendPutAckToRequestor(message);
		} else {
			assert(0);
		}
		break;
	default :
		assert(0);
		break;

	}
}

void MSIDirectoryController::state_M_Handler(MSIMessage * message) {
	MSIDirectoryEntry & directoryEntry = directory[message->getAddress()];
	switch (message->getType()) {
	case MSIMessage::GETS :
		sendFwdGetSToOwner(message);
		directoryEntry.addSharer(message->getRequestor());
		directoryEntry.addSharer(directoryEntry.getOwner());
		directoryEntry.setOwner(nullptr);
		changeEntryState(message->getAddress(), MSIDirectoryEntry::S_D);
		break;
	case MSIMessage::GETM :
		sendFwdGetMToOwner(message);
		directoryEntry.setOwner(message->getRequestor());
		break;
	case MSIMessage::PUTS :
		sendPutAckToRequestor(message);
		break;
	case MSIMessage::PUTM :
		if (message->isFromOwner()) {
			directoryEntry.setOwner(nullptr);
			changeEntryState(message->getAddress(), MSIDirectoryEntry::I);
		}
		sendPutAckToRequestor(message);
		break;
	default :
		assert(0);
		break;

	}
}

void MSIDirectoryController::state_S_D_Handler(MSIMessage * message) {
	MSIDirectoryEntry & directoryEntry = directory[message->getAddress()];
	switch (message->getType()) {
	case MSIMessage::GETS :
		assert(0);
		break;
	case MSIMessage::GETM :
		assert(0);
		break;
	case MSIMessage::PUTS :
		directoryEntry.removeSharer(message->getRequestor());
		sendPutAckToRequestor(message);
		break;
	case MSIMessage::PUTM :
		if (message->isFromOwner()) {
			directoryEntry.removeSharer(message->getRequestor());
			sendPutAckToRequestor(message);
		} else {
			assert(0);
		}
		break;
	case MSIMessage::DATA :
		changeEntryState(message->getAddress(), MSIDirectoryEntry::S);
		break;
	default :
		assert(0);
		break;
	}
}
