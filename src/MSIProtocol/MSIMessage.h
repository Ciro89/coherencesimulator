
#pragma once

#include <iostream>
#include <string.h>

#include "../SystemComponent.h"
#include "../Message.h"

class MSIMessage : public Message {
public:

	typedef enum {
		//Events from Core
		LOAD,
		STORE,
		REPLACEMENT,

		//Requests
		GETS,
		GETM,
		PUTS,
		PUTM,

		//Forwarded Requests
		FWD_GETS,
		FWD_GETM,
		INV,
		PUT_ACK,

		//Response
		DATA,
		INV_ACK,

		MAX
	} MessageType;

	MSIMessage() = default;
	MSIMessage(SystemComponent * recipient, MessageType type, unsigned int address);
	MSIMessage(SystemComponent * sender, SystemComponent * recipient, SystemComponent * requestor, MessageType type, unsigned int address);
	virtual ~MSIMessage() = default;


	SystemComponent * getRequestor() const;
	MessageType getType() const;
	bool isFromOwner() const;
	bool isFromDirectory() const;
	unsigned int getAckCount() const;
	unsigned int getAddress() const;


	void setRequestor(SystemComponent * requestor);
	void setType(MessageType type);
	void isFromOwner(bool isFromOwner);
	void isFromDirectory(bool isFromDirectory);
	void setAckCount(unsigned int ackCount);
	void setAddress(unsigned int address);

	virtual std::string toString() const;
	friend std::ostream & operator<<(std::ostream & os, const MSIMessage & message);


private:

	SystemComponent * requestor = nullptr;

	MessageType type = LOAD;
	bool fromOwner = false;
	bool fromDirectory = false;
	unsigned int ackCount = 0;
	unsigned int address = 0;


	static const char * const TYPE_TO_STR[];

};


