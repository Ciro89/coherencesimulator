/*
 * InterconnectionNetwork.h
 *
 *  Created on: 01 dic 2016
 *      Author: cirod
 */

#pragma once

#include <map>
#include <queue>
#include "SimulatorFramerowk/Simulator.h"
#include "SimulatorFramerowk/NotificationHandler.h"
#include "Message.h"

class InterconnectionNetwork : public SystemComponent {
public:

	InterconnectionNetwork(std::string name, Simulator * simulator);
	InterconnectionNetwork(const InterconnectionNetwork &) = delete;
	virtual ~InterconnectionNetwork();

	const InterconnectionNetwork & operator=(const InterconnectionNetwork &) = delete;

	void sendMessage(Message * message);
	Message * getMessage(SystemComponent * receiver);
	void popMessage(SystemComponent * receiver);
	bool isThereMessage(SystemComponent * receiver) const;
	void clean();

	virtual void notify(Notification * notification);

private:

	class InterconnectionNotification : public Notification {
	public:
		InterconnectionNotification() = delete;
		InterconnectionNotification(double time, NotificationHandler * recipient, Message * message) : Notification(time, recipient), message(message) {};

		Message * getMessage() const {return message;}

	private:
		Message * message;
	};

	std::map<
		NotificationHandler*,
		double
	> lastCycle;

	std::map<
		NotificationHandler*,
		std::queue<Message* >
	> messageQueues;

};


