/*
 * SystemComponent.h
 *
 *  Created on: 07 dic 2016
 *      Author: ciro
 */

#pragma once

#include <string>
#include "SimulatorFramerowk/NotificationHandler.h"

class SystemComponent : public NotificationHandler {
public:

	SystemComponent() = default;
	SystemComponent(std::string name) : NotificationHandler(), name(name) {};
	SystemComponent(std::string name, Simulator * simulator) : name(name), simulator(simulator) {};
	SystemComponent(const SystemComponent & systemComponent) = default;
	virtual ~SystemComponent() = default;

	std::string getName() const {return name;}
	void setName(std::string name) {this->name = name;};
	Simulator * getSimulator() const {return simulator;}
	void setSimulator() {this->simulator = simulator;}

	virtual void notify(Notification * notification) = 0;

protected:
	std::string name = "";
	Simulator * simulator = nullptr;
};

