/*
 * CoherenceDomain.h
 *
 *  Created on: 12 dic 2016
 *      Author: ciro
 */

#pragma once

#include <map>
#include <vector>
#include "SimulatorFramerowk/Simulator.h"


class CoherenceDomain {
public:

	CoherenceDomain(Simulator * simulator);
	virtual ~CoherenceDomain() = default;

	void incrementReaders(unsigned int address);
	void incrementWriters(unsigned int address);
	void decrementReaders(unsigned int address);
	void decrementWriters(unsigned int address);
	void check(unsigned int address);

private:

	typedef struct {
		unsigned int read_only = 0;
		unsigned int read_write = 0;
	} NodeCount;

	std::map<unsigned int,  NodeCount> globalState;
	Simulator * simulator;
};



