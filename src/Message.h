/*
 * Message.h
 *
 *  Created on: 01 dic 2016
 *      Author: cirod
 */

#pragma once

#include <sstream>
#include "SystemComponent.h"

class Message {
public:

	struct MessageComparator {
		bool operator() (const Message * left, const Message * right) const;
	};

	Message() = default;
	Message(SystemComponent * recipient);
	Message(SystemComponent * sender, SystemComponent * recipient);
	Message(const Message & message) = default;
	virtual ~Message();

	virtual std::string toString() const;

	void setSender(SystemComponent * sender);
	void setRecipient(SystemComponent * recipient);
	SystemComponent * getSender() const;
	SystemComponent * getRecipient() const;

protected:

	SystemComponent * sender = nullptr;
	SystemComponent * recipient = nullptr;

};

