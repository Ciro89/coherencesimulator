#include <iostream>
#include <assert.h>
#include "InterconnectionNetwork.h"

InterconnectionNetwork::InterconnectionNetwork(std::string name, Simulator * simulator) :
	SystemComponent(name, simulator),
	lastCycle(),
	messageQueues() {}

InterconnectionNetwork::~InterconnectionNetwork() {
	clean();
}

void InterconnectionNetwork::sendMessage(Message * message) {
	double & lastTime = lastCycle[message->getRecipient()];	//TODO: Controllare se ritorna 0 quando non c'è la chiave cercata
	double nextTime = simulator->getTime() + 1;
	if (lastTime >= nextTime)
		nextTime = lastTime + 1;	//FIXME: Parametrizzare
	lastTime = nextTime;
	simulator->scheduleEvent(new InterconnectionNotification(nextTime, this, message));
}

Message * InterconnectionNetwork::getMessage(SystemComponent * receiver) {
	assert(!messageQueues.at(receiver).empty());
	return messageQueues.at(receiver).front();	//Lancia un 'eccezione
}

void InterconnectionNetwork::popMessage(SystemComponent * receiver) {
	assert(!messageQueues.at(receiver).empty());
	messageQueues.at(receiver).pop();
}

bool InterconnectionNetwork::isThereMessage(SystemComponent * receiver) const {
	try {
		return messageQueues.at(receiver).empty();
	} catch (std::out_of_range & e) {
		return false;
	}
}

void InterconnectionNetwork::clean() {
	for (auto it = messageQueues.begin(); it != messageQueues.end(); it++) {
		while (!(it->second.empty())) {
			delete it->second.front();
			it->second.pop();
		}
	}
}

void InterconnectionNetwork::notify(Notification * notification) {
	InterconnectionNotification & n = dynamic_cast<InterconnectionNotification&>(*notification);	//TODO: Lancia un'eccezione badcast
	messageQueues[n.getMessage()->getRecipient()].push(n.getMessage());
	simulator->scheduleEvent(new Notification(simulator->getTime() + 1, this, n.getMessage()->getRecipient()));	//FIXME:Parametrizzare
}

