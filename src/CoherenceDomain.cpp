/*
 * CoherenceDomain.cpp
 *
 *  Created on: 12 dic 2016
 *      Author: ciro
 */

#include "CoherenceDomain.h"

CoherenceDomain::CoherenceDomain(Simulator * simulator) :
	simulator(simulator) {}

void CoherenceDomain::incrementReaders(unsigned int address) {
	globalState[address].read_only++;
}

void CoherenceDomain::incrementWriters(unsigned int address) {
	globalState[address].read_write++;
}

void CoherenceDomain::decrementReaders(unsigned int address) {
	globalState[address].read_only--;
}

void CoherenceDomain::decrementWriters(unsigned int address) {
	globalState[address].read_write--;
}

void CoherenceDomain::check(unsigned int address) {
	if ((globalState[address].read_write > 1) || ((globalState[address].read_write == 1) && (globalState[address].read_only > 0)))
		simulator->stop();
}

